# Python TM/TC USLP

To be written...

## Installation

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/prototypes/python-tmtc.git
$ cd python-tmtc
$ virtualenv venv
$ . venv/bin/activate
$ pip install -e .
$ pip install git+https://gitlab.com/librecube/prototypes/python-sdlp.git@016efa508fbd6fbae5d6486b989cff680d719ef5
```

## Example

To be written...

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-tmtc/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-tmtc

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.

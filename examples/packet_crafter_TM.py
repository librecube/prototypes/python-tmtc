from tmtc.pdu.TmFrame import TmFrame, TmFramePrimaryHeader, TransferFrameDataFieldStatus
from tmtc.tools import hexdump
from tmtc.packet_crafter.pack_TM import pack_TM


def encode_tm(
    raw_data=b"Test_Packets" * 2,
    MC_count=0,
    VC_count=0,
    DF_len=10,
    VCID=6,
    Fixed_size=True,
):

    print(2 * "\n" + 20 * "=" + "TM Frame encoding " + 20 * "=" + 2 * "\n")

    TF_Data_status = TransferFrameDataFieldStatus(tf_sec_hdr_flag=0, synch_flag=1)

    tmFramePrimaryHeader = TmFramePrimaryHeader(
        scid=123,
        vcid=VCID,
        ocf_flag=0,
        mc_frame_count=MC_count,
        vc_frame_count=VC_count,
        transferFrameDataFieldStatus=TF_Data_status,
        tfvn=0b00,
    )
    """
    The above is the config of the
    first packet of a SDU from a given
    VC
    """

    attrs = vars(tmFramePrimaryHeader)
    print(
        "\n Primary header of first packet of series\n",
        ", ".join("%s: %s" % item for item in attrs.items()),
    )
    attrs = vars(tmFramePrimaryHeader.transferFrameDataFieldStatus)

    print(
        "\n Transfer Frame Data Field Status \n",
        ",".join("%s: %s" % item for item in attrs.items()),
    )

    config = pack_TM(
        sdu=raw_data,
        packet_size=DF_len,
        TM_Primary_header=tmFramePrimaryHeader,
        TM_Secondary_Header=None,
        fixed_size=Fixed_size,
        mc_count=tmFramePrimaryHeader.mc_frame_count,
        vc_count=tmFramePrimaryHeader.vc_frame_count,
    )
    result = config.craft_packets()  # Creates a packet sequence
    return result


def decode_tm(packets):
    decoded_frames = []
    print(2 * "\n" + 20 * "=" + "TM Frame decoded " + 20 * "=" + 2 * "\n")
    for data in packets:
        tmFrameDecoded = TmFrame.decode(data)
        attrs = vars(tmFrameDecoded.tmFramePrimaryHeader)
        print(
            "\n Primary header \n\n",
            ", ".join("%s: %s" % item for item in attrs.items()),
        )
        attrs = vars(tmFrameDecoded.tmFramePrimaryHeader.transferFrameDataFieldStatus)

        print(
            "\n Transfer Frame Data Field Status \n\n",
            ", ".join("%s: %s" % item for item in attrs.items()),
        )
        print("\n Data \n")
        hexdump(tmFrameDecoded.tmFrameDataField)
        decoded_frames.append(tmFrameDecoded)
    return decoded_frames


if __name__ == "__main__":
    packets, mc_count, _ = encode_tm()
    decode_tm(packets)
    packets2, _, _ = encode_tm(
        raw_data=b"CCSDS" * 9,
        MC_count=mc_count,
        VC_count=0,
        DF_len=25,
        VCID=5,
        Fixed_size=False,
    )
    decode_tm(packets2)

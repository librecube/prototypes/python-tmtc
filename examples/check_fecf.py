from tmtc.pdu.TmFrame.TmFrameTrailer import TmFrameErrorCtrlField
from tmtc.tools import hexdump
from tmtc.machine import crcCheck


def check_fecf_ErrorFree():
    databytes = bytes([1, 2, 3, 4, 5, 6, 7, 8])

    databytes += TmFrameErrorCtrlField.createFecf(databytes)
    # This returns the fecf in bytes which we just need to append
    # to the TmFrame

    print('Frame sent:')
    hexdump(databytes)

    DataReceived = databytes
    print('Frame received:')
    hexdump(DataReceived)

    return not crcCheck(DataReceived)


def check_fecf_Noisy():

    databytes = bytes([1, 2, 3, 4, 5, 6, 7, 8])

    databytes += TmFrameErrorCtrlField.createFecf(databytes)
    # This returns the fecf in bytes which we just need to append
    # to the TmFrame

    print('Frame sent:')
    hexdump(databytes)

    # Now lets make some changes to imitate errors in transmission
    DataReceived = b'23' + databytes[2:]

    print('Frame received:')
    hexdump(DataReceived)

    return not crcCheck(DataReceived)


if __name__ == "__main__":
    print("Frame Error Status: " + str(check_fecf_ErrorFree()))
    print("Frame Error Status: " + str(check_fecf_Noisy()))

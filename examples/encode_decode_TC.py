from tmtc.pdu import TcFramePrimaryHeader, TcFrame
from tmtc.tools import hexdump

data = bytearray()


def encode_tc():
    bypass_flag = 1
    ctrl_cmd_flag = 0
    scid = 123
    vcid = 34
    frame_seq_num = 14
    tfdata = b"packets" * 20

    print(2 * "\n" + 20 * "=" + "TC Frame encoding " + 20 * "=" + 2 * "\n")
    tcFramePrimaryHeader = TcFramePrimaryHeader(
        bypass_flag, ctrl_cmd_flag, scid, vcid, frame_seq_num
    )
    attrs = vars(tcFramePrimaryHeader)
    print(
        "\n Primary header \n\n", ", ".join("%s: %s" % item for item in attrs.items())
    )
    tcFrame = TcFrame(tcFramePrimaryHeader, tfdata)
    print("\n Data \n")
    hexdump(tfdata)

    data.extend(tcFrame.encode())


def decode_tc():

    print(2 * "\n" + 20 * "=" + "TC Frame decoded " + 20 * "=" + 2 * "\n")
    tcFrameDecoded = TcFrame.decode(data)
    attrs = vars(tcFrameDecoded.tcFramePrimaryHeader)
    print("\n Primary header \n\n", ",".join("%s: %s" % item for item in attrs.items()))
    print("\n Data \n")
    hexdump(tcFrameDecoded.tcFrameDataField)


if __name__ == "__main__":
    encode_tc()
    decode_tc()

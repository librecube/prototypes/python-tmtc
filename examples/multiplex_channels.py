import threading
import time
from tmtc.pdu.TmFrame import TmFramePrimaryHeader, TmFrame, TransferFrameDataFieldStatus
from tmtc.machine import multiplex
from tmtc.tools import hexdump


class VCtest:
    def __init__(self):
        self.a = 5
        self.start_time = time.time()
        self.duration = 20  # Duration of action for thread control

        self.multiplexer = multiplex(  # init selector VCIDs
            [
                1,  # High prority VCID for eg. House keeping data
                3,  # Nominal priority packets
                2,
            ]  # Least priority packets
        )  # VCIDs in order of pref. of packet priority

        self.vc_count = {1: 0, 2: 0, 3: 0}  # Dictionary to keep vc count
        self.mc_count = 0

    def start(self):
        vc1_thread = threading.Thread(target=self.vc1)
        vc2_thread = threading.Thread(target=self.vc2)
        vc3_thread = threading.Thread(target=self.vc3)
        sender_thread = threading.Thread(target=self.sender)
        vc1_thread.start()
        vc2_thread.start()
        vc3_thread.start()
        sender_thread.start()

    def vc1(self):  # Example virtual channel 1
        TF_Data_status = TransferFrameDataFieldStatus(tf_sec_hdr_flag=0, synch_flag=1)

        tmFramePrimaryHeader = TmFramePrimaryHeader(
            scid=123,
            vcid=1,
            ocf_flag=0,
            mc_frame_count=0,
            vc_frame_count=0,
            transferFrameDataFieldStatus=TF_Data_status,
            tfvn=0b00,
        )  # Template 1st header
        while self.start_time + self.duration > time.time():
            tmFrame = TmFrame(tmFramePrimaryHeader, b"channel1" * 20)
            self.multiplexer.insert(  # Inserts frame in multiplexer
                tmFrame.encode(), tmFramePrimaryHeader.vcid
            )
            time.sleep(5)  # Generates packets every 5 sec

    def vc2(self):
        TF_Data_status = TransferFrameDataFieldStatus(tf_sec_hdr_flag=0, synch_flag=1)

        tmFramePrimaryHeader = TmFramePrimaryHeader(
            scid=123,
            vcid=2,
            ocf_flag=0,
            mc_frame_count=0,
            vc_frame_count=0,
            transferFrameDataFieldStatus=TF_Data_status,
            tfvn=0b00,
        )
        while self.start_time + self.duration > time.time():
            tmFrame = TmFrame(tmFramePrimaryHeader, b"channel2" * 20)
            self.multiplexer.insert(tmFrame.encode(), tmFramePrimaryHeader.vcid)
            time.sleep(1)

    def vc3(self):
        TF_Data_status = TransferFrameDataFieldStatus(tf_sec_hdr_flag=0, synch_flag=1)

        tmFramePrimaryHeader = TmFramePrimaryHeader(
            scid=123,
            vcid=3,
            ocf_flag=0,
            mc_frame_count=0,
            vc_frame_count=0,
            transferFrameDataFieldStatus=TF_Data_status,
            tfvn=0b00,
        )
        while self.start_time + self.duration > time.time():
            tmFrame = TmFrame(tmFramePrimaryHeader, b"channel3" * 20)
            self.multiplexer.insert(tmFrame.encode(), tmFramePrimaryHeader.vcid)
            time.sleep(3)

    def sender(self):
        while self.start_time + self.duration > time.time():
            if not self.multiplexer.isEmpty():  # Checks for queued packets
                packet, vcid = self.multiplexer.delete()  # gives packet & VCID
                tmFrameDecoded = TmFrame.decode(packet)  # Decodes packet
                tmFrameDecoded.tmFramePrimaryHeader.vc_frame_count = self.vc_count[
                    vcid
                ]  # Init vc_count using VCID dict
                tmFrameDecoded.tmFramePrimaryHeader.mc_frame_count = (
                    self.mc_count
                )  # Init master channel count
                self.mc_count += 1  # Increase MC count
                self.vc_count[vcid] += 1  # Increase specific VC count
                attrs = vars(tmFrameDecoded.tmFramePrimaryHeader)
                print(
                    "\n Primary header \n\n",
                    ", ".join("%s: %s" % item for item in attrs.items()),
                )
                print("\n Data \n")
                hexdump(tmFrameDecoded.tmFrameDataField)


if __name__ == "__main__":
    engine = VCtest()
    engine.start()

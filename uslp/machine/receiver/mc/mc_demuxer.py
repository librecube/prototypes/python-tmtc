from collections import defaultdict


class MasterChannelDemuxer:
    def __init__(self, frames):
        self.frames = frames
        self.mc_demuxed = defaultdict(list)  # Dictionary to store all MC demuxed frames
    
    def demux(self):
        for frame in self.frames:
            mcid = frame.primary_header.version + frame.primary_header.spacecraft_id
            self.mc_demuxed[mcid].append(frame)
        return self.mc_demuxed

from collections import defaultdict


class MapDemuxer:
    def __init__(self, extracted_data):  # MapId, FHP and TFDZ from one VC Channel
        self.extracted_data = extracted_data
        self.map_demuxed = defaultdict(list)  # Dictionary to store all MC demuxed frames
    
    def demux(self):
        for data in self.extracted_data:
            map_id = data.map_id
            self.map_demuxed[map_id].append(data)
        return self.map_demuxed

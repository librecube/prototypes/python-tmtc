class MappExtraction:
    def __init__(self, map_id, map_demuxed):
        self.map_id = map_id
        self.map_demuxed = map_demuxed
    
    def extractPackets(self):
        packets = []
        packet = bytes()
        for data in self.map_demuxed[self.map_id]:
            if data.fhp == 0xffff:
                packet += data.tfdz
            elif data.fhp == 0:
                if len(packet) != 0:
                    packets.append(packet)
                packet = data.tfdz
            else:
                packet += data.tfdz[:data.fhp]
                packets.append(packet)
                packet = data.tfdz[data.fhp:]
        return packets

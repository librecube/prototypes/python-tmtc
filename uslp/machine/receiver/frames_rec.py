from uslp.machine.crc16 import crcCheck
import sdlp

TRUNCATED_FRAME_LENGTH = 12  # Assigned a random value


class FramesRec:
    def __init__(self, data):
        self.data = data
        self.uslp_frames = []

    def decodeIntoFrames(self):
        '''
        Splitting data into frames if it passes crcCheck
        '''
        while len(self.data) > 0:
            if self.data[3] & 0x01:
                if crcCheck(self.data[:TRUNCATED_FRAME_LENGTH]):
                    self.uslp_frames.append(self._parse_frame(self.data[:TRUNCATED_FRAME_LENGTH], 1, True))
                self.data = self.data[TRUNCATED_FRAME_LENGTH:]
            else:
                length = int.from_bytes(self.data[4:6], "big")
                if crcCheck(self.data[:length+1]):
                    self.uslp_frames.append(self._parse_frame(self.data[:length+1], 1))
                self.data = self.data[length+1:]
        return self.uslp_frames

    def _parse_frame(frame_bytes, vc_frame_count_len, truncated=False):
        if not truncated:
            primary_header = sdlp.UslpFramePrimaryHeaderStruct.parse(frame_bytes[:7+vc_frame_count_len])
            frame_bytes = frame_bytes[7+vc_frame_count_len:]
            transfer_frame_data_field_header = sdlp.UslpFrameDataFieldHeaderStruct.parse(frame_bytes[:3])
            frame_bytes = frame_bytes[3:]
            if primary_header.ocf_flag:                
                tfdz = frame_bytes[:-6]
                frame_bytes = frame_bytes[-6:]
                operational_control_field = frame_bytes[:4]
                frame_bytes = frame_bytes[4:]
                frame_error_control_field = frame_bytes
            else:
                tfdz = frame_bytes[:-2]
                frame_bytes = frame_bytes[-2:]
                frame_error_control_field = frame_bytes
        else:
            raise NotImplementedError("Truncated frame decoding not implemented yet")
        return sdlp.UslpTransferFrame(
            primary_header=primary_header,
            transfer_frame_data_field_header=transfer_frame_data_field_header,
            transfer_frame_data_zone=tfdz,
            operational_control_field=operational_control_field,
            frame_error_control_field=frame_error_control_field
        )



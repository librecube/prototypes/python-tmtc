from collections import defaultdict


class VirtualChannelDemuxer:
    def __init__(self, frames):  # Frames from one MCID
        self.frames = frames
        self.vc_demuxed = defaultdict(list)  # Dictionary to store all MC demuxed frames
    
    def demux(self):
        for frame in self.frames:
            vcid = frame.primary_header.virtual_channel_id
            self.vc_demuxed[vcid].append(frame)
        return self.vc_demuxed

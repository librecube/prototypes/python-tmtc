class MapIdSDU:
    def __init__(self, map_id, fhp, sdu):
        self.map_id = map_id
        self.fhp = fhp
        self.sdu = sdu


class VirtualChannelReception:
    def __init__(self, frames):  # Frames of one vcid
        self.frames = frames

    def extractSDU(self):
        extracted_data = []
        for frame in self.frames:
            extracted_data.append(MapIdSDU(frame.primary_header.map_id, frame.transfer_frame_data_field_header.first_header_or_last_valid_octet_pointer, frame.transfer_frame_data_zone))

        return extracted_data

class MapMuxer:

    def __init__(self, mapidSeq: list):
        self.queue = []
        self.mapidSeq = mapidSeq

    def isEmpty(self):
        return len(self.queue) == 0

    def insert(self, mapp_processor, mapid):
        if mapid in self.mapidSeq:
            self.queue.append((mapp_processor, mapid))

    def delete(self):
        try:
            max = 0
            for i in range(len(self.queue)):
                if self.mapidSeq.index(self.queue[i][1]) < self.mapidSeq.index(
                    self.queue[max][1]
                ):
                    max = i
            item = self.queue[max]
            del self.queue[max]
            return item
        except IndexError:
            print()
            exit()

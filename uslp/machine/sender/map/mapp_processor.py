FIXED_TFDZ_LENGTH = 65526


class Fhp_Tfdz:
    def __init__(self, fhp, tfdz):
        self.fhp = fhp  # First header pointer
        self.tfdz = tfdz


class MapPacketProcessor:
    def __init__(self, pvn):
        self.last_tfdz_space = FIXED_TFDZ_LENGTH
        self.tfdz_list = [Fhp_Tfdz(0, bytes())]*1
        self.pvn = pvn

    def _calc_last_tfdz_space(self):
        return FIXED_TFDZ_LENGTH - len(self.tfdz_list[-1].tfdz)

    def _add_new_tfdz(self, fhp):
        self.tfdz_list.append(Fhp_Tfdz(fhp, bytes()))
        self.last_tfdz_space = FIXED_TFDZ_LENGTH

    def add_packet(self, mapp):
        if mapp.pvn != self.pvn:
            raise ValueError("Expected pvn: "+str(self.pvn))
        packet_bytes = mapp.packet.encode()
        while len(packet_bytes) > 0:
            if self.last_tfdz_space == 0:
                if len(packet_bytes) == mapp.packet.__len__:
                    self._add_new_tfdz(0)
                elif len(packet_bytes) > FIXED_TFDZ_LENGTH:
                    self._add_new_tfdz(0xffff)
                else:
                    self._add_new_tfdz(len(packet_bytes))

            if len(packet_bytes) < self.last_tfdz_space:
                self.last_tfdz_space -= len(packet_bytes)
                self.tfdz_list[-1].tfdz += packet_bytes
                packet_bytes = bytes()
            elif len(packet_bytes) > self.last_tfdz_space:
                self.tfdz_list[-1].tfdz += packet_bytes[:self.last_tfdz_space]
                packet_bytes = packet_bytes[self.last_tfdz_space:]
                self.last_tfdz_space = self._calc_last_tfdz_space()
            else:
                self.tfdz_list[-1].tfdz += packet_bytes
                self.last_tfdz_space = self._calc_last_tfdz_space()
                packet_bytes = bytes()

    def add_idle_packets(self):
        if self.last_tfdz_space == 0:
            return
        # TODO(): Add idle encapsulation packets to fill up the last tfdz



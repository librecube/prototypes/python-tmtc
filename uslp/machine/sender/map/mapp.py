class MapPacket:
    def __init__(self, packet, gmap_id, pvn, sdu_id, qos):
        self.packet = packet
        self.gmap_id = gmap_id
        self.pvn = pvn
        self.sdu_id = sdu_id
        self.qos = qos

    def request(self, packet, gmap_id, pvn, sdu_id, qos):
        pass

    def notify_indication(self, gmap_id, pvn, sdu_id, qos, notification_type):
        pass

    def indication(self, packet, gmap_id, pvn):
        pass

class MasterChannelGenerator:
    def __init__(self, mcid):
        self.mcid = mcid
        self.virtual_channels = []

    def generateFrames(self, muxed_queue):
        while not muxed_queue.isEmpty():
            processed_vc = muxed_queue.delete()
            self.virtual_channels.append(processed_vc.virtual_channel)
        return self.virtual_channels

class MasterChannelMuxer:

    def __init__(self, mcidSeq: list):
        self.queue = []
        self.mcidSeq = mcidSeq

    def isEmpty(self):
        return len(self.queue) == 0

    def insert(self, master_channel, mcid):
        if mcid in self.vcidSeq:
            self.queue.append((master_channel, mcid))

    def delete(self):
        try:
            max = 0
            for i in range(len(self.queue)):
                if self.mcidSeq.index(self.queue[i][1]) < self.mcidSeq.index(
                    self.queue[max][1]
                ):
                    max = i
            item = self.queue[max]
            del self.queue[max]
            return item
        except IndexError:
            print()
            exit()

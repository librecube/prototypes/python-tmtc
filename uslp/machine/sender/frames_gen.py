class FramesGen:
    def __init__(self, mc_muxed):
        self.mc_muxed = mc_muxed
        self.frames = bytes([])
    
    def generateFrames(self):
        while not self.mc_muxed.isEmpty():
            mc = self.mc_muxed.delete()
            for vc in mc.master_channel.virtual_channels:
                for uslp_frame in vc.uslp_frames:
                    self.frames += (uslp_frame)
        return self.frames

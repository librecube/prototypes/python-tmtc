class VirtualChannelMuxer:

    def __init__(self, vcidSeq: list):
        self.queue = []
        self.vcidSeq = vcidSeq

    def isEmpty(self):
        return len(self.queue) == 0

    def insert(self, virtual_channel, vcid):
        if vcid in self.vcidSeq:
            self.queue.append((virtual_channel, vcid))

    def delete(self):
        try:
            max = 0
            for i in range(len(self.queue)):
                if self.vcidSeq.index(self.queue[i][1]) < self.vcidSeq.index(
                    self.queue[max][1]
                ):
                    max = i
            item = self.queue[max]
            del self.queue[max]
            return item
        except IndexError:
            print()
            exit()

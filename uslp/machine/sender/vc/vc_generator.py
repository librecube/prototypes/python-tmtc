import sdlp
from uslp.machine.crc16 import crc16

virtual_channel_frame_count_length = 1  # Max 255


class VirtualChannelGenerator:
    def __init__(self, version, spacecraft_id, source_destination_id, virtual_channel_id, end_of_frame_primary_header_flag, length, bypass_sequence_control_flag, protocol_control_command_flag, spare, ocf_flag):
        self.version = version
        self.spacecraft_id = spacecraft_id
        self.source_destination_id = source_destination_id
        self.virtual_channel_id = virtual_channel_id
        self.end_of_frame_primary_header_flag = end_of_frame_primary_header_flag
        self.bypass_sequence_control_flag = bypass_sequence_control_flag
        self.protocol_control_command_flag = protocol_control_command_flag
        self.spare = spare
        self.ocf_flag = ocf_flag
        self.vc_frame_count = 0
        self.uslp_frames = []
    
    def _calc_frame_len(self, tfdz, vc_frame_count_len, ocf_flag=False):
        return 7 + vc_frame_count_len + len(tfdz) + (4 if ocf_flag else 0) + 2 - 1

    def generateFrames(self, muxed_queue):
        while not muxed_queue.isEmpty():
            self.vc_frame_count += 1
            if self.vc_frame_count > 255:
                raise NotImplementedError("Creation of multiple VCs")
            processed_map = muxed_queue.delete()
            for fhp_tfdz in processed_map.mapp_processor.tfdz_list:

                primary_header = sdlp.UslpFramePrimaryHeaderStruct.build(dict(
                    version=self.version,
                    spacecraft_id=self.spacecraft_id,
                    source_destination_id=self.source_destination_id,
                    virtual_channel_id=self.virtual_channel_id,
                    map_id=processed_map.map_id,
                    end_of_frame_primary_header_flag=self.end_of_frame_primary_header_flag,
                    length=self._calc_frame_len(fhp_tfdz.tfdz, virtual_channel_frame_count_length),
                    bypass_sequence_control_flag=self.bypass_sequence_control_flag,
                    protocol_control_command_flag=self.protocol_control_command_flag,
                    spare=self.spare,
                    ocf_flag=self.ocf_flag,
                    virtual_channel_frame_count_length=virtual_channel_frame_count_length,
                    virtual_channel_frame_count=self.vc_frame_count,
                ))

                transfer_frame_data_field_header = sdlp.UslpFrameDataFieldHeaderStruct.build(dict(
                    tfdz_construction_rules=0b000,
                    uslp_protocol_id=1,
                    first_header_or_last_valid_octet_pointer=fhp_tfdz.fhp
                ))

                transfer_frame_data_zone = fhp_tfdz.tfdz

                # Assuming ocf_flag = 0 for simplicity
                frame_error_control_field = crc16((primary_header + transfer_frame_data_field_header + transfer_frame_data_zone))

                uslpFrame = primary_header + transfer_frame_data_field_header + transfer_frame_data_zone + frame_error_control_field
                self.uslp_frames.append(uslpFrame)
        return self.uslp_frames

def crc16(data: bytes, poly=0x11021):
    """
    CRC-16-CCITT Algorithm

    Default polinomial according to CCSDS is
    G(X) = X^16 + X^12 + X^5 + 1
    CRC-16 code is provided in subsection 9.4 in
    CCSDS 130.1-G-3
    https://public.ccsds.org/Pubs/130x1g3.pdf

    """
    data = bytearray(data)
    crc = 0xFFFF
    for b in data[0:32751]:  # Maximum CRC outcome for 32751 bytes
        cur_byte = 0xFF & b
        for _ in range(0, 8):
            if (crc & 0x0001) ^ (cur_byte & 0x0001):
                crc = (crc >> 1) ^ poly
            else:
                crc >>= 1
            cur_byte >>= 1
    crc = ~crc & 0xFFFF
    crc = (crc << 8) | ((crc >> 8) & 0xFF)
    return crc & 0xFFFF


def crcCheck(data: bytes):
    """
    Checks CRC status of bytes
    """
    status = (int.from_bytes(data[-2:],'big') == crc16(data[:-2]))

    # Checks if trailing bits match crc

    return(status)

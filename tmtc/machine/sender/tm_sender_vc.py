from tmtc.pdu.TmFrame import TmFrame, TmFramePrimaryHeader, TransferFrameDataFieldStatus
from tmtc.pdu.TmFrame.TmFrameTrailer import TmFrameErrorCtrlField
from tmtc.machine.mux import multiplex


class TmSenderVC:
    def __init__(self, scid, vcid, frameLen, mux):
        self.scid = scid
        self.vcid = vcid
        self.frameLen = frameLen
        self.mux = mux  # Needs dictionary of vcids to be initialized
        self.currentFrame = None
        self.current_vc_frame_count = 1

    def putPktsInFrames(self, packets):
        """
        Input is a list of spacepackets which will be segmented
        into frames of fixed length self.frameLen
        frames are then multiplexed into virtual channels and self.mux is returned.
        """
        maxUserDataLen = TmFrame.getUserDataLen(self.frameLen, 0, 0)
        freespace = maxUserDataLen
        for pkt in packets:
            currentPktLen = pkt.packet_data_length
            pktData = pkt.encode()
            while currentPktLen > 0:
                if self.currentFrame is None:
                    self.currentFrame = TmFrame()
                    self.currentFrame.tmFramePrimaryHeader = TmFramePrimaryHeader(
                        self.scid,
                        self.vcid,
                        0,
                        0,
                        self.current_vc_frame_count,
                        TransferFrameDataFieldStatus(0, 0),
                    )
                if currentPktLen == pkt.packet_data_length:
                    if freespace >= currentPktLen:
                        freespace -= currentPktLen
                        currentPktLen = 0
                        if self.currentFrame.tmFrameDataField is None:
                            self.currentFrame.tmFrameDataField = pktData
                        else:
                            self.currentFrame.tmFrameDataField += pktData
                    else:
                        # Needs splitting
                        if freespace != 0:
                            if self.currentFrame.tmFrameDataField is None:
                                self.currentFrame.tmFrameDataField = pktData[:freespace]
                            else:
                                self.currentFrame.tmFrameDataField += pktData[
                                    :freespace
                                ]
                            currentPktLen -= freespace
                        self.mux.insert(
                            self.currentFrame,
                            self.currentFrame.tmFramePrimaryHeader.vcid,
                        )
                        self.currentFrame = TmFrame()
                        self.current_vc_frame_count += 1
                        if self.current_vc_frame_count < 256:
                            self.currentFrame.tmFramePrimaryHeader = (
                                TmFramePrimaryHeader(
                                    self.scid,
                                    self.vcid,
                                    0,
                                    0,
                                    self.current_vc_frame_count,
                                    TransferFrameDataFieldStatus(0, 0),
                                )
                            )
                            freespace = maxUserDataLen
                        else:
                            raise NotImplementedError()
                else:
                    # Segmented packet
                    if freespace >= currentPktLen:
                        freespace -= currentPktLen
                        currentPktLen = 0
                        if self.currentFrame.tmFrameDataField is None:
                            self.currentFrame.tmFrameDataField = pktData[
                                -currentPktLen:
                            ]
                        else:
                            self.currentFrame.tmFrameDataField += pktData[
                                -currentPktLen:
                            ]
                    else:
                        # Needs splitting
                        if freespace != 0:
                            if self.currentFrame.tmFrameDataField is None:
                                self.currentFrame.tmFrameDataField = pktData[:freespace]
                            else:
                                self.currentFrame.tmFrameDataField += pktData[
                                    :freespace
                                ]
                            currentPktLen -= freespace
                        self.mux.insert(
                            self.currentFrame,
                            self.currentFrame.tmFramePrimaryHeader.vcid,
                        )
                        self.currentFrame = TmFrame()
                        self.current_vc_frame_count += 1
                        if self.current_vc_frame_count < 256:
                            self.currentFrame.tmFramePrimaryHeader = (
                                TmFramePrimaryHeader(
                                    self.scid,
                                    self.vcid,
                                    0,
                                    0,
                                    self.current_vc_frame_count,
                                    TransferFrameDataFieldStatus(0, 0),
                                )
                            )
                            freespace = maxUserDataLen
                        else:
                            raise NotImplementedError()
        return self.mux

from tmtc.pdu.TmFrame import TmFrame
import spp


class sync_receiver:
    """
    To be written

    """
    def __init__(self, vcid: int, scid: int, TF_length: int,
                 idle_data=b'00'):
        self.TF_packets = []
        self.SPP_packets = []
        self.vcid = vcid
        self.scid = scid
        self.TF_length = TF_length
        self.spp_buffer = b''
        self.idle_data = idle_data

    def verify(self, packet: bytes):
        tmFrameDecoded = TmFrame.decode(packet)
        if((tmFrameDecoded.tmFramePrimaryHeader.scid == self.scid) and (
                tmFrameDecoded.tmFramePrimaryHeader.vcid == self.vcid)):
            return True
        else:
            return False

    def add_TF_packets(self, packets: list(bytes)):
        for packet in packets:
            if(self.verify(packet)):
                self.TF_packets.append(packet)
                
        self.extract_spp()

    def get_SPP(self):
        if(len(self.SPP_packets) == 0):
            Idle_Packet = spp.SpacePacket(
                packet_type=spp.PacketType.TELEMETRY,
                packet_sec_hdr_flag=False,
                apid=2047,          # Apid for Idle packets
                sequence_flags=spp.SequenceFlags.UNSEGMENTED,
                packet_sequence_count=0,
                packet_data_field=self.idle_data  # mission specific
                )

            return Idle_Packet  # return Idle packet To be implemented
        else:
            item = self.SPP_packets[0]
            del self.SPP_packets[0]
            return item

    def extract_spp(self,):
        while(len(self.TF_packets) != 0):
            curr_frame = self.TF_packets.pop()
            tmFrameDecoded = TmFrame.decode(curr_frame)
            header_pointer = tmFrameDecoded.tmFramePrimaryHeader.\
                transferFrameDataFieldStatus.first_hdr_ptr
            if(header_pointer == 2047):  # no header
                self.buffer = self.buffer + tmFrameDecoded.tmFrameDataField
            else:
                if(header_pointer > 0):
                    self.buffer = self.buffer + \
                        tmFrameDecoded.tmFrameDataField[:header_pointer]
                    self.SPP_packets.append(self.buffer)
                    self.buffer = b''
                    #  endpointer = header_pointer
                    self.buffer = tmFrameDecoded.tmFrameDataField[
                        header_pointer:]

                """ while(len(tmFrameDecoded.tmFrameDataField) <= endpointer):
                    spp.SpacePacket.decode( """
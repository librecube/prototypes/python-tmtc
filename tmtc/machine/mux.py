class multiplex:
    """

    The mux expects the user to  initialize
    a list of valid vcids in order of decreasing
    packet priority. Each delete operation yeilds
    the packet with highest priority first.

    The time complexity of delete is O(n)

    An example implementation of multiplex operation
    is as follows.
    if __name__ == '__main__':
        myQueue = multiplex([23,13,12])
        myQueue.insert(b'dfvafdvaadfv',13)
        myQueue.insert(b'efvefv',12)
        myQueue.insert(b'qefvqefv',23)
        myQueue.insert(b'qwefqwefqwfe',12)
        while not myQueue.isEmpty():
            print(myQueue.delete())
    """

    def __init__(self, vcidSeq: list):
        self.queue = []
        self.vcidSeq = vcidSeq

    def isEmpty(self):
        return len(self.queue) == 0

    def insert(self, sdu, vcid):
        if vcid in self.vcidSeq:
            self.queue.append((sdu, vcid))

    def delete(self):
        try:
            max = 0
            for i in range(len(self.queue)):
                if self.vcidSeq.index(self.queue[i][1]) < self.vcidSeq.index(
                    self.queue[max][1]
                ):
                    max = i
            item = self.queue[max]
            del self.queue[max]
            return item
        except IndexError:
            print()
            exit()

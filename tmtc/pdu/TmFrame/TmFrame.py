from .TmFrameHeader import (
    TmFramePrimaryHeader,
    TransferFrameDataFieldStatus,
    TmFrameSecondaryHeader,
)
from .TmFrameTrailer import TmFrameErrorCtrlField, TmFrameTrailer


class TmFrame:
    def __init__(
        self,
        tmFramePrimaryHeader=None,
        tmFrameDataField=None,
        tmFrameSecondaryHeader=None,
        tmFrameTrailer=None,
    ):
        self.tmFramePrimaryHeader = tmFramePrimaryHeader
        self.tmFrameDataField = tmFrameDataField
        self.tmFrameSecondaryHeader = tmFrameSecondaryHeader
        self.tmFrameTrailer = tmFrameTrailer

    def encode(self):
        if self.tmFramePrimaryHeader is None:
            raise AttributeError("Primary header cannot be None Type")
        databytes = bytes(
            [
                (self.tmFramePrimaryHeader.tfvn << 6)
                + (self.tmFramePrimaryHeader.scid >> 4),
                ((self.tmFramePrimaryHeader.scid & 0xF) << 4)
                + (self.tmFramePrimaryHeader.vcid << 1)
                + (self.tmFramePrimaryHeader.ocf_flag),
                (self.tmFramePrimaryHeader.mc_frame_count),
                (self.tmFramePrimaryHeader.vc_frame_count),
                (
                    self.tmFramePrimaryHeader.transferFrameDataFieldStatus.tf_sec_hdr_flag
                    << 7
                )
                + (
                    self.tmFramePrimaryHeader.transferFrameDataFieldStatus.synch_flag
                    << 6
                )
                + (
                    self.tmFramePrimaryHeader.transferFrameDataFieldStatus.pkt_odr_flag
                    << 5
                )
                + (
                    self.tmFramePrimaryHeader.transferFrameDataFieldStatus.seg_len_id
                    << 3
                )
                + (
                    self.tmFramePrimaryHeader.transferFrameDataFieldStatus.first_hdr_ptr
                    >> 8
                ),
                (
                    self.tmFramePrimaryHeader.transferFrameDataFieldStatus.first_hdr_ptr
                    & 0xFF
                ),
            ]
        )

        if self.tmFramePrimaryHeader.transferFrameDataFieldStatus.tf_sec_hdr_flag:
            databytes += bytes(
                [
                    self.tmFrameSecondaryHeader.tf_sec_hdr_ver_no
                    << 6 + self.tmFrameSecondaryHeader.tf_sec_hdr_data_len
                ]
            )
            databytes += self.tmFrameSecondaryHeader.tf_sec_hdr_data_field

        if self.tmFrameDataField is None:
            raise AttributeError("Datafield cannot be None Type")
        databytes += self.tmFrameDataField

        # TODO: Implement Operational Control Field

        # Appending the frame error control field
        databytes += TmFrameErrorCtrlField.createFecf(databytes)

        return databytes

    def getUserDataLen(frameLen, sec_hdr_len, ocf_flag):
        return (
            frameLen
            - 6
            - ((sec_hdr_len + 1) if sec_hdr_len != 0 else 0)
            - (4 if ocf_flag else 0)
            - 2
        )

    @classmethod
    def decode(cls, tmframe):
        tfvn = tmframe[0] >> 6
        scid = ((tmframe[0] & 0x3F) << 4) + (tmframe[1] >> 4)
        vcid = (tmframe[1] >> 1) & 0x7
        ocf_flag = tmframe[1] & 0x1
        mc_frame_count = tmframe[2]
        vc_frame_count = tmframe[3]
        tf_sec_hdr_flag = tmframe[4] >> 7
        synch_flag = (tmframe[4] >> 6) & 0x1
        pkt_odr_flag = (tmframe[4] >> 5) & 0x1
        seg_len_id = (tmframe[4] >> 3) & 0x3
        first_hdr_ptr = ((tmframe[4] & 0x7) << 8) + tmframe[5]

        transferFrameDataFieldStatus = TransferFrameDataFieldStatus(
            tf_sec_hdr_flag, synch_flag, pkt_odr_flag, seg_len_id, first_hdr_ptr
        )
        tmFramePrimaryHeader = TmFramePrimaryHeader(
            scid,
            vcid,
            ocf_flag,
            mc_frame_count,
            vc_frame_count,
            transferFrameDataFieldStatus,
            tfvn,
        )
        tmFrameSecondaryHeader = None
        tmFrameDataField = None
        if tf_sec_hdr_flag:
            tf_sec_hdr_ver_no = tmframe[6] >> 6
            tf_sec_hdr_data_len = tmframe[6] & 0x3F
            tf_sec_hdr_data_field = tmframe[7 : 7 + tf_sec_hdr_data_len]
            tmFrameSecondaryHeader = (tf_sec_hdr_ver_no, tf_sec_hdr_data_field)
            tmFrameDataField = tmframe[7 + tf_sec_hdr_data_len :]
        else:
            tmFrameDataField = tmframe[6:]  # assuming trailer is absent

        # TODO: Implement Operational Control Field

        fecf = TmFrameErrorCtrlField(int.from_bytes(tmframe[-2:], "big"))
        tmFrameTrailer = TmFrameTrailer(frameErrorCtrlField=fecf)

        return cls(
            tmFramePrimaryHeader,
            tmFrameDataField,
            tmFrameSecondaryHeader,
            tmFrameTrailer,
        )

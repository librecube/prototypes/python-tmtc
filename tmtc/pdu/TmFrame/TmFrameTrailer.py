from tmtc.machine import crc16


# TODO: Implement Operational Control Field


class TmFrameErrorCtrlField:
    # Frame error control field is a 16-bit number
    def __init__(self, fecf):
        self.fecf = fecf

    def createFecf(databytes):
        return int.to_bytes(crc16(databytes), byteorder="big", length=2)

    def checkFecf(tmFrame):
        frameCrc = crc16(tmFrame[:-2])
        fecf = int.from_bytes(tmFrame[-2:], "big")
        return frameCrc == fecf


class TmFrameTrailer:
    def __init__(self, operationalCtrlField=None, frameErrorCtrlField=None):
        self.operationalCtrlField = operationalCtrlField
        self.frameErrorCtrlField = frameErrorCtrlField

class TransferFrameDataFieldStatus:
    def __init__(
        self,
        tf_sec_hdr_flag,
        synch_flag,
        pkt_odr_flag=None,
        seg_len_id=None,
        first_hdr_ptr=None,
    ):
        self.tf_sec_hdr_flag = tf_sec_hdr_flag
        self.synch_flag = synch_flag
        if not synch_flag:
            self.pkt_odr_flag = 0
            self.seg_len_id = 3
            if not tf_sec_hdr_flag:
                # TODO: Check for idle and no packets
                self.first_hdr_ptr = 7
        else:
            self.pkt_odr_flag = 0
            self.seg_len_id = 0
            self.first_hdr_ptr = 0


class TmFramePrimaryHeader:
    def __init__(
        self,
        scid,
        vcid,
        ocf_flag,
        mc_frame_count,
        vc_frame_count,
        transferFrameDataFieldStatus,
        tfvn=0b00,
    ):
        self.tfvn = tfvn
        self.scid = scid
        self.vcid = vcid
        self.ocf_flag = ocf_flag
        self.mc_frame_count = mc_frame_count
        self.vc_frame_count = vc_frame_count
        self.transferFrameDataFieldStatus = transferFrameDataFieldStatus


class TmFrameSecondaryHeader:
    def __init__(self, tf_sec_hdr_ver_no, tf_sec_hdr_data_field):
        self.tf_sec_hdr_data_field = tf_sec_hdr_data_field
        self.tf_sec_hdr_data_len = len(tf_sec_hdr_data_field)
        self.tf_sec_hdr_data_field = tf_sec_hdr_data_field

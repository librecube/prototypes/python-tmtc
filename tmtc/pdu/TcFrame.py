class TcFramePrimaryHeader:
    def __init__(
        self,
        bypass_flag,
        ctrl_cmd_flag,
        scid,
        vcid,
        frame_seq_num,
        tfvn=0b00,
        rsvd_spare=0b00,
        frame_len=0,
    ):
        self.bypass_flag = bypass_flag
        self.ctrl_cmd_flag = ctrl_cmd_flag
        self.scid = scid
        self.vcid = vcid
        self.frame_seq_num = frame_seq_num
        self.tfvn = tfvn
        self.rsvd_spare = rsvd_spare
        self.frame_len = frame_len


class TcFrame:
    def __init__(self, tcFramePrimaryHeader, tcFrameDataField, frameErrCtrlField=None):
        self.tcFramePrimaryHeader = tcFramePrimaryHeader
        self.tcFrameDataField = tcFrameDataField
        self.frameErrCtrlField = frameErrCtrlField

    def encode(self):

        self.tcFramePrimaryHeader.frame_len = (
            len(self.tcFrameDataField) + 5 + (2 if (self.frameErrCtrlField) else 0) - 1
        )
        databytes = bytes(
            [
                (self.tcFramePrimaryHeader.tfvn << 6)
                + (self.tcFramePrimaryHeader.bypass_flag << 5)
                + (self.tcFramePrimaryHeader.ctrl_cmd_flag << 4)
                + (self.tcFramePrimaryHeader.rsvd_spare << 2)
                + (self.tcFramePrimaryHeader.scid >> 8),
                (self.tcFramePrimaryHeader.scid & 0xFF),
                (self.tcFramePrimaryHeader.vcid << 2)
                + (self.tcFramePrimaryHeader.frame_len >> 8),
                (self.tcFramePrimaryHeader.frame_len & 0xFF),
                (self.tcFramePrimaryHeader.frame_seq_num),
            ]
        )

        databytes += self.tcFrameDataField
        # TODO: Frame Control Field
        return databytes

    @classmethod
    def decode(cls, tcframe):
        tfvn = tcframe[0] >> 6
        bypass_flag = (tcframe[0] >> 5) & 0x01
        ctrl_cmd_flag = (tcframe[0] >> 4) & 0x01
        rsvd_spare = (tcframe[0] >> 2) & 0x03
        scid = ((tcframe[0] & 0x03) << 8) + tcframe[1]
        vcid = tcframe[2] >> 2
        frame_len = ((tcframe[2] & 0x03) << 8) + tcframe[3]
        frame_seq_num = tcframe[4]
        tcFramePrimaryHeader = TcFramePrimaryHeader(
            bypass_flag,
            ctrl_cmd_flag,
            scid,
            vcid,
            frame_seq_num,
            tfvn,
            rsvd_spare,
            frame_len,
        )
        tcFrameDataField = tcframe[5:frame_len]
        # TODO: Frame Control Field

        return cls(tcFramePrimaryHeader, tcFrameDataField)

from tmtc.pdu import TcFrame


class pack_TC:
    def __init__(
        self, sdu, packet_size, TC_Primary_header, fixed_size=False, frame_seq_num=0
    ):
        self.sdu = bytearray(sdu)
        self.packet_size = packet_size
        self.fixed_size = fixed_size
        self.frame_seq_num = frame_seq_num
        self.TC_Primary_header = TC_Primary_header
        self.packets = []

    def craft_packets(self):
        while self.sdu:
            self.TC_Primary_header.frame_seq_num = self.frame_seq_num
            self.frame_seq_num += 1 % 0xFF
            if len(self.sdu) < self.packet_size and self.fixed_size:
                """
                For fixed size packets append
                the remaining bytes with 0
                """
                self.TC_Primary_header.frame_len = self.packet_size
                packet = TcFrame(
                    self.TC_Primary_header,
                    bytes(
                        self.sdu[: self.packet_size]
                        + bytearray(self.packet_size - len(self.sdu))
                    ),
                )
                self.packets.append(packet.encode())
            else:
                self.TC_Primary_header.frame_len = len(self.sdu[: self.packet_size])
                packet = TcFrame(
                    self.TC_Primary_header, bytes(self.sdu[: self.packet_size])
                )
                self.packets.append(packet.encode())

            del self.sdu[: self.packet_size]
        return (self.packets, self.frame_seq_num)

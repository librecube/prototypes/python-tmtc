from tmtc.pdu import TmFrame


class pack_TM:
    def __init__(
        self,
        sdu,
        packet_size,
        TM_Primary_header,
        TM_Secondary_Header=None,
        fixed_size=False,
        mc_count=0,
        vc_count=0,
    ):
        self.sdu = bytearray(sdu)
        self.packet_size = packet_size
        self.fixed_size = fixed_size
        self.mc_count = mc_count
        self.vc_count = vc_count
        self.TM_Primary_header = TM_Primary_header
        self.TM_Secondary_Header = TM_Secondary_Header
        self.packets = []

    def craft_packets(self):
        while self.sdu:
            self.TM_Primary_header.mc_frame_count = self.mc_count
            self.TM_Primary_header.vc_frame_count = self.vc_count
            self.mc_count += 1 % 0xFF
            self.vc_count += 1 % 0xFF
            if len(self.sdu) < self.packet_size and self.fixed_size:
                """
                For fixed size packets append
                the remaining bytes with 0
                """
                packet = TmFrame(
                    self.TM_Primary_header,
                    bytes(
                        self.sdu[: self.packet_size]
                        + bytearray(self.packet_size - len(self.sdu))
                    ),
                    self.TM_Secondary_Header,
                )
                self.packets.append(packet.encode())
            else:
                packet = TmFrame(
                    self.TM_Primary_header,
                    bytes(self.sdu[: self.packet_size]),
                    self.TM_Secondary_Header,
                )
                self.packets.append(packet.encode())

            del self.sdu[: self.packet_size]
        return (self.packets, self.mc_count, self.vc_count)

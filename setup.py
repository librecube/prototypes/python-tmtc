from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="tmtc_data_link",
    version="0.0.1",
    author_email="info@librecube.org",
    description="CCSDS TM/TC Space Data Link Protocols",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/librecube/prototypes/python-tmtc",
    license="MIT",
    python_requires=">=3.4",
    packages=find_packages(exclude=["examples", "tests", "docs"]),
)
